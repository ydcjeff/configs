// @ts-check
/** @type {import('eslint').Rule.RuleModule} */
module.exports = {
	meta: {
		type: 'suggestion',
	},
	create(ctx) {
		let old = null;

		const priority = {
			none: 0,
			default: 1,
			star: 2,
			named: 3,
			type_default: 4,
			type_star: 5,
			type_named: 6,
		};

		function get_sort_index(node) {
			// none import (i.e. import 'style.css')
			if (node.specifiers.length === 0) {
				return priority.none;
			}
			// default import (i.e. import fs from 'fs')
			if (node.specifiers[0].type === 'ImportDefaultSpecifier') {
				// type import (i.e. import type TypeC from 'abc')
				if (node?.importKind === 'type') return priority.type_default;
				return priority.default;
			}
			// namespace import (i.e. import * as path from 'path')
			if (node.specifiers[0].type === 'ImportNamespaceSpecifier') {
				// type import (i.e. import type * as TypeC from 'abc')
				if (node?.importKind === 'type') return priority.type_star;
				return priority.star;
			}
			// normal import (i.e. import { ref }  from 'vue')
			if (node.specifiers[0].type === 'ImportSpecifier') {
				// type import (i.e. import type { TypeC } from 'abc')
				if (node?.importKind === 'type') return priority.type_named;
				return priority.named;
			}
			return 99;
		}

		function get_module_name(node) {
			return node.source.value;
		}

		const message = `Expected "{{ new }}" before "{{ old }}"`;

		return {
			ImportDeclaration(node) {
				if (old) {
					const new_index = get_sort_index(node);
					const old_index = get_sort_index(old);

					const new_module = get_module_name(node);
					const old_module = get_module_name(old);

					if (new_index === old_index && new_module < old_module) {
						ctx.report({
							node,
							message,
							data: {
								old: old_module,
								new: new_module,
							},
						});
					} else if (new_index < old_index) {
						ctx.report({
							node,
							message,
							data: {
								old: old_module,
								new: new_module,
							},
						});
					}

					const import_specifiers = node.specifiers
						.filter((v) => v.type === 'ImportSpecifier')
						.map((v) => v.imported.name);

					import_specifiers.forEach((v, i, a) => {
						const old = a[i - 1];
						if (old > v) {
							ctx.report({
								node,
								message,
								data: {
									old,
									new: v,
								},
							});
						}
					});
				}
				old = node;
			},
		};
	},
};
