module.exports = {
	rules: {
		isort: require('./isort.js'),
		esort: require('./esort.js'),
	},
};
