// @ts-check
/** @type {import('eslint').Rule.RuleModule} */
module.exports = {
	meta: {
		type: 'suggestion',
	},
	create(ctx) {
		let old = null;

		const priority = {
			star: 0,
			named: 1,
			default: 2,
			type_named: 3,
		};

		function is_re_export(node) {
			return !!node.source || (node.specifiers && node.specifiers.length > 0);
		}

		function get_sort_index(node) {
			// export * from 'module'
			if (node.type === 'ExportAllDeclaration') {
				return priority.star;
			}
			// export { named } from 'module'
			if (node.type === 'ExportNamedDeclaration') {
				if (node?.importKind === 'type') return priority.type_named;
				return priority.named;
			}
			// export default abc
			if (node.type === 'ExportDefaultDeclaration') {
				return priority.default;
			}
			return 99;
		}

		function get_module_name(node) {
			return node.source?.value;
		}

		const message = `Expected "{{ new }}" before "{{ old }}"`;

		function esort(node) {
			if (old && is_re_export(old) && is_re_export(node)) {
				const new_index = get_sort_index(node);
				const old_index = get_sort_index(old);

				const new_module = get_module_name(node);
				const old_module = get_module_name(old);
				if (new_index === old_index && new_module < old_module) {
					ctx.report({
						node,
						message,
						data: {
							old: old_module,
							new: new_module,
						},
					});
				} else if (new_index < old_index) {
					ctx.report({
						node,
						message,
						data: {
							old: old_module,
							new: new_module,
						},
					});
				}

				if (node.specifiers) {
					const export_specifiers = node.specifiers
						.filter((v) => v.type === 'ExportSpecifier')
						.map((v) => v.local.name);

					export_specifiers.forEach((v, i, a) => {
						const old = a[i - 1];
						if (old > v) {
							ctx.report({
								node,
								message,
								data: {
									old,
									new: v,
								},
							});
						}
					});
				}
			}
			old = node;
		}

		return {
			ExportAllDeclaration(node) {
				esort(node);
			},
			ExportNamedDeclaration(node) {
				esort(node);
			},
			ExportDefaultDeclaration(node) {
				esort(node);
			},
		};
	},
};
