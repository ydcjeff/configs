# @ydcjeff/configs

```sh
pnpm add @ydcjeff/configs -D
```

## Prettier

```json
{
	"prettier": "@ydcjeff/configs/prettier"
}
```

## ESLint

```js
module.exports = {
	extends: [require.resolve('@ydcjeff/configs/eslint')],
};
```

> **Note**
>
> It needs to provide full resolved path to ESLint since ESLint loads its
> plugins by naming convention.

## TSconfig

```json
{
	"extends": "@ydcjeff/configs/tsconfig"
}
```
